﻿using UnityEngine;
using System.Collections;

public class BGHexaController : MonoBehaviour {
    private bool colorChanging = false;
    private bool disabled = true;
    private bool fadeIn = true;

    private Color beginColor;
    private Color endColor;
    private Color lerpedColor;

    private float delay;
    private float duration;
    private float smoothness = 0.1f;

    private float minAlpha;
    private float maxAlpha;

	// Use this for initialization
	void Start () {
        InitColor();
        Invoke("StartChange", delay);
	}
	
	// Update is called once per frame
	void Update () {
        UpdateColor();
	}

    void StartChange()
    {
        disabled = false;
    }

    void InitColor()
    {
        beginColor = GetComponent<MeshRenderer>().material.color;

        minAlpha = Random.Range(1, 3) * 0.001f;
        maxAlpha = Random.Range(15, 25) * 0.001f;
        beginColor.a = minAlpha;

        endColor = beginColor;
        endColor.a = maxAlpha;

        duration = Random.Range(155, 350) * 0.01f;
        delay = Random.Range(5, 100) * 0.01f;

        GetComponent<MeshRenderer>().material.color = beginColor;
    }

    void UpdateColor()
    {
        if (!colorChanging && !disabled)
        {
            if (fadeIn)
                StartCoroutine(FadeIn());
            else
                StartCoroutine(FadeOut());
        }

        GetComponent<MeshRenderer>().material.color = lerpedColor;
    }

    IEnumerator FadeIn()
    {
        colorChanging = true;

        float _progress = 0;
        float _inc = smoothness / duration;

        while (_progress < 1)
        {
            lerpedColor = Color.Lerp(beginColor, endColor, _progress);
            _progress += _inc;
            yield return new WaitForSeconds(smoothness);
        }

        fadeIn = !fadeIn;
        colorChanging = false;
    }

    IEnumerator FadeOut()
    {
        colorChanging = true;

        float _progress = 0;
        float _inc = smoothness / duration;

        while (_progress < 1)
        {
            lerpedColor = Color.Lerp(endColor, beginColor, _progress);
            _progress += _inc;
            yield return new WaitForSeconds(smoothness);
        }

        fadeIn = !fadeIn;
        colorChanging = false;
    }
}
