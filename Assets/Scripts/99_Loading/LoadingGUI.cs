﻿/*
 ********************************************************************************* 
 * This script creates the on screen display of the Loading (scene transition)
 *
 * created on: 	
 * 		09-Jun-2014	Raymond Wong
 * Updates:
 ********************************************************************************* 
 */

using UnityEngine;
using System.Collections;

public class LoadingGUI : MonoBehaviour {
	//****************************
	//*                          *
	//*     Public Variables     *
	//*                          *
	//****************************
	//GUI settings
	public GUISkin skin;

	public float dHeight = 1136.0f;
	public float dWidth = 640.0f;

	public Rect loadingRect;
	public Texture2D loadingTexture;
	
	void OnGUI () {
		//Configures the transform matrix for the GUI drawing
		GUI.matrix = Matrix4x4.TRS (new Vector3(0, 0, 0), Quaternion.identity, new Vector3(Screen.width / dWidth, Screen.height / dHeight, 1));
		
		//Load the GUISkin from Resources
		GUI.skin = skin;

		//Draw the loading texture
		GUI.DrawTexture(loadingRect, loadingTexture);
	}
}
