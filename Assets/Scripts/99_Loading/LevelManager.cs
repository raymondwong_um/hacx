﻿/*
 ********************************************************************************* 
 * This script creates a loading transition screen before next scene is loaded
 * 
 * created on: 	
 * 		09-Jun-2014	Raymond Wong
 * Updates:
 ********************************************************************************* 
 */

using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	//*****************************
	//*                           *
	//*     Private Functions     *
	//*                           *
	//*****************************
	IEnumerator InnerLoad(string sceneName) {
		//Load transition scene
		Object.DontDestroyOnLoad (this.gameObject);
		Application.LoadLevel ("Loading");
		
		yield return new WaitForSeconds (1f);
		
		//Load the target scene
		Application.LoadLevel (sceneName);
		Destroy (gameObject);
	}

	//****************************
	//*                          *
	//*     Public Functions     *
	//*                          *
	//****************************
	/// <summary>
	/// Load the specified sceneName.
	/// </summary>
	/// <param name="sceneName">Scene name.</param>
	public static void Load(string sceneName) {
		GameObject go = new GameObject ("LevelManager");
		LevelManager instance = go.AddComponent<LevelManager> ();
		instance.StartCoroutine (instance.InnerLoad (sceneName));
	}

}
