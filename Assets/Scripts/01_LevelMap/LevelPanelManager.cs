﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelPanelManager : MonoBehaviour {
    private Transform head;

	// Use this for initialization
	void Start () {
        //InitFullMap();
        InitCurrentMap();
	}
	
	// Update is called once per frame
	void Update () {
        //ScrollRect _sr = transform.parent.GetComponent<ScrollRect>();
        //if (_sr == null)
        //    Debug.Log("Shit");
        //else
        //    Debug.Log("ScrollPos=" + _sr.verticalNormalizedPosition);

	}

    void InitFullMap()
    {
        UnityEngine.Object[] _files = Resources.LoadAll("XML/00_Levels/");

        for (int i = _files.Length - 1; i > -1; i--)
        {
            string _fname = _files[i].name.Substring(4);

            GameObject _lvl;
            if (i > 0)
            {
                _lvl = Instantiate(Resources.Load("Prefabs/00_LevelMap/Level_Body")) as GameObject;
                _lvl.transform.FindChild("toggle_select").GetComponent<Toggle>().group = head.GetComponent<ToggleGroup>();
            }
            else
            {
                _lvl = Instantiate(Resources.Load("Prefabs/00_LevelMap/Level_Head")) as GameObject;
                head = _lvl.transform.FindChild("toggle_select");
            }

            _lvl.transform.SetParent(transform);
            _lvl.transform.FindChild("stage_name").GetComponent<Text>().text = _fname;
            _lvl.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    void InitCurrentMap()
    {
        int _currentLv = 9;

        if (!string.IsNullOrEmpty(PlayerPrefs.GetString("CurrentLeveLName")))
            _currentLv = int.Parse(PlayerPrefs.GetString("CurrentLeveLName"));

        if (_currentLv < 6)
        {
            int _null_count = 5 - _currentLv;
            for (int n = 0; n < _null_count; n++)
            {
                GameObject _null = Instantiate(Resources.Load("Prefabs/00_LevelMap/Level_Null")) as GameObject;
                _null.transform.SetParent(transform);
                _null.transform.localScale = new Vector3(1, 1, 1);
            }
        }

        for (int i = _currentLv; i > -1; i--)
        {
            GameObject _lvl;
            if (i > 0)
                _lvl = Instantiate(Resources.Load("Prefabs/00_LevelMap/Level_Body")) as GameObject;
            else
                _lvl = Instantiate(Resources.Load("Prefabs/00_LevelMap/Level_Head")) as GameObject;

            _lvl.transform.SetParent(transform);
            _lvl.transform.FindChild("stage_name").GetComponent<Text>().text = i.ToString("000");
            _lvl.transform.localScale = new Vector3(1, 1, 1);
        }
    }
}
