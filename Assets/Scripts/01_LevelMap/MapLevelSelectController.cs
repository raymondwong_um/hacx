﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MapLevelSelectController : MonoBehaviour {
    private string levelName = "000";

    private MapUIController mc;

	// Use this for initialization
	void Start () {
        mc = transform.root.GetComponent<MapUIController>();
        levelName = transform.parent.FindChild("stage_name").GetComponent<Text>().text;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SelectLevel()
    {
        mc.SetSelectedLevelName(levelName);
    }

    public void ToggleLevel()
    {
        if (GetComponent<Toggle>().isOn)
            mc.SetSelectedLevelName(levelName);

        mc.SetToggled(GetComponent<Toggle>());
    }
}
