﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MapUIController : MonoBehaviour {
    private string selectedLevelName = "000";

    private Text stageNameText;

    private Toggle toggle;

	// Use this for initialization
	void Start () {
        stageNameText = transform.FindChild("stage_name").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        stageNameText.text = "Stage " + selectedLevelName;
	}

    public void SetSelectedLevelName(string _name)
    {
        selectedLevelName = _name;
    }

    public void SetToggled(Toggle _tg)
    {
//      if (toggle != null && toggle!=_tg)
//          toggle.isOn = false;

//      toggle = _tg;

    }

    public void StartLevel()
    {
        PlayerPrefs.SetString("LevelName", selectedLevelName);
        Application.LoadLevel("02_LevelScene");
    }
}
