﻿using UnityEngine;
using System.Collections;

public class HexaManager : MonoBehaviour {
    //**************************
    //*   ROTATION variables   *
    //**************************
    public bool isAntiCW = false;
    public bool isLocked = false;
    public bool isRotating = false;

    private float rotateSpeed = 5f;

    public int totalMove = 0;

    private Quaternion currentRotation;
    private Quaternion newRotation;
    private Quaternion oldRotation;

    private Vector3 newAngle;

    private float timer = 0.0f;

    //***********************
    //*   FADER variables   *
    //***********************
    public float invokeWait = 1f;

    private float duration = 0.3f;
    private float duration_mask = 0.2f;
    private float smoothness = 0.01f;

    private bool fadeCompleted = false;
    private bool isMaskFading = false;

    //Model Colors
    private Color modelLerpedColor;
    private Color modelBeginColor;
    private Color modelEndColor;

    //Sprite Colors
    private Color maskLerpedColor;
    private Color maskBeginColor;
    private Color maskEndColor;

    private MeshRenderer mr;

    private SpriteRenderer sr;

    //***************************
    //*   HEXA FACE variables   *
    //***************************
    /* -1 = disabled
     * 0 = unselected
     * 1 = selected
     */
    public int status = -1;

    private SpriteRenderer face;

    //*******************
    //*   HEXA STATUS   *
    //*******************
    public bool hexaLinked = false;

    void Start()
    {
        if (status > -1)
        {
            InitHexa();
            InitFace();
        }
        else
        {
            HideHexa();
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdateModelColor();
        UpdateSideStatus();
        UpdateHexaFace();
        ProgressRotation();
    }

    //*************************
    //*     Fader methods     *
    //*************************
    void HideHexa()
    {
        transform.FindChild("hexa_face").gameObject.SetActive(false);
    }

    void InitHexa()
    {
        mr = GetComponent<MeshRenderer>();
        sr = transform.FindChild("hexa_mask").gameObject.GetComponent<SpriteRenderer>();

        modelBeginColor = mr.material.color;
        modelBeginColor.a = 0f;

        maskBeginColor = transform.FindChild("hexa_mask").gameObject.GetComponent<SpriteRenderer>().color;
        maskEndColor = maskBeginColor;
        maskEndColor.a = 0;

        Color _color = modelBeginColor;
        _color.a = 0.1f;
        modelEndColor = _color;

        Invoke("StartFade", invokeWait);
    }

    void UpdateModelColor()
    {
        if (status > -1 && !fadeCompleted)
        {
            mr.material.color = modelLerpedColor;

            if (isMaskFading)
                sr.color = maskLerpedColor;
        }
    }

    void StartFade()
    {
        //Debug.Log("[HexaFader][StartFade] Check Point [00]");
        StartCoroutine(FadeIn());
    }

    IEnumerator FadeIn()
    {
        float _progress = 0;
        float _inc = smoothness / duration;

        while (_progress < 1)
        {
            modelLerpedColor = Color.Lerp(modelBeginColor, modelEndColor, _progress);
            _progress += _inc;
            yield return new WaitForSeconds(smoothness);
        }

        isMaskFading = true;
        transform.FindChild("hexa_mask").gameObject.SetActive(true);
        transform.FindChild("hexa_face").gameObject.SetActive(true);

        StartCoroutine(FadeInMask());
    }

    IEnumerator FadeInMask()
    {
        float _progress = 0;
        float _inc = smoothness / duration_mask;

        while (_progress < 1)
        {
            maskLerpedColor = Color.Lerp(maskBeginColor, maskEndColor, _progress);
            _progress += _inc;
            yield return new WaitForSeconds(smoothness);
        }

        fadeCompleted = true;
        //upon fading completed, enable links
        for (int _i = 1; _i < 7; _i++)
            transform.FindChild("hexa_face").FindChild(_i.ToString("00") + "_side").GetComponent<HexaSideTrigger>().isDisabled = false;
    }

    //****************************
    //*     Rotation Methods     *
    //****************************

    /// <summary>
    /// If not isLocked, prepare the rotation information, once all details are ready
    /// set the boolean isRotating to true and Update() will automatically
    /// proceed the actual model rotation with ProgressRotation()
    /// </summary>
    /// <param name="_isAntiCW">Determine the rotation direction</param>
    public void RotateHexa(bool _isAntiCW)
    {
        if (!isRotating && !isLocked)
        {
            currentRotation = Quaternion.Euler(transform.localEulerAngles);
            oldRotation = currentRotation;

            newAngle = transform.localEulerAngles;

            if (!_isAntiCW)
                newAngle.y += 60;
                //newAngle.z += 60;
            else
                newAngle.y -= 60;
                //newAngle.z += 60;

            newRotation = Quaternion.Euler(newAngle);

            totalMove++;
            isRotating = true;
        }
    }

    /// <summary>
    /// Proceed with the actual rotation of the heagon model
    /// Using Quaternion.Lerp to give a smooth rotation visual effect
    /// </summary>
    private void ProgressRotation()
    {
        if (isRotating)
        {
            timer += Time.deltaTime;

            transform.localRotation = Quaternion.Lerp(currentRotation, newRotation, timer * rotateSpeed);

            if (Mathf.Abs(transform.localEulerAngles.y - oldRotation.eulerAngles.y) >= 58 && Mathf.Abs(transform.localEulerAngles.y - oldRotation.eulerAngles.y) <= 62)
            {
                transform.localRotation = newRotation;

                isRotating = false;
                timer = 0.0f;
            }

            if (Mathf.Abs(transform.localEulerAngles.y - oldRotation.eulerAngles.y) <= 302 && Mathf.Abs(transform.localEulerAngles.y - oldRotation.eulerAngles.y) >= 298)
            {
                isRotating = false;
                timer = 0.0f;
            }
        }
    }

    //*********************************
    //*     Hexa Face controllers     *
    //*********************************
    void InitFace()
    {
        face = transform.FindChild("hexa_face").GetComponent<SpriteRenderer>();
    }

    void UpdateHexaFace()
    {
        switch (status)
        {
            case 0:
                face.sprite = (Sprite) Resources.Load<Sprite>("Sprites/00_Levels/Hexa_01_normal");
                break;

            case 1:
                face.sprite = (Sprite) Resources.Load<Sprite>("Sprites/00_Levels/Hexa_01_focus") as Sprite;
                break;
        }
    }

    //***********************************
    //*     Hexa Status controllers     *
    //***********************************
    void UpdateSideStatus()
    {
        if (status > -1)
        {
            HexaSideTrigger[] _hs = transform.FindChild("hexa_face").GetComponentsInChildren<HexaSideTrigger>();

            bool _linked = false;
            foreach (HexaSideTrigger _h in _hs)
            {
                if (!_h.isLinked)
                {
                    _linked = false;
                    break;
                }
                _linked = true;
            }
            hexaLinked = _linked;
        }
        else
        {
            hexaLinked = true;
        }
    }
}
