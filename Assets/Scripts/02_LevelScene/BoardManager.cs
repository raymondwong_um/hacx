﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Xml;
using System.Collections;

public class BoardManager : MonoBehaviour
{
    public bool isLocked = false;

    public GameObject selectedHexa;
    public TouchLineRenderer touchLineRenderer;

    public HexaEngine engine;

    public int controlPref = 0;         //0=button; 1=swipe
    public int moveCount = 0;

    public string levelName = "008";

	public Text label_levelName;

    //*********************
    //*   Debug display   *
    //*********************
    public Text debug_selectedHexa;
    //public Text debug_rotDir;
    public Text debug_touchSide;
    public Text debug_posDelta;
    //public Text debug_angle;
    public Text debug_beginDir;
    public Text debug_touchDir;
    //public Text debug_radian;

    private bool allLinked = false;
    //private bool angleSet = false;
    private bool ccw = false;       //counter-clockwise
    private bool hasMoved = false;  //prevent selecting hexa when rotating
    private bool touchSide = true;  //true = right; false = left

    private float touchAngle = 0f;
	private float thresholdAngle = 20f;

    private HexaContainerManager[] containers;

    private Ray ray;
    private RaycastHit rayhit;

    private Vector3 beginDir = Vector3.zero;
    private Vector3 touchDir = Vector3.zero;

    private Touch touch;

    // Use this for initialization
    void Start()
    {
        levelName = PlayerPrefs.GetString("LevelName");
        if (string.IsNullOrEmpty(levelName))
            levelName = "008";

		label_levelName.text = string.Format("Stage {0}", levelName);

        CreateBoard();

        containers = GetComponentsInChildren<HexaContainerManager>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckLinks();

        if (!isLocked)
            SelectHexa();
    }

    void CheckLinks()
    {
        bool _linked = true;
        foreach (HexaContainerManager _hc in containers)
        {
            if (!_hc.allHexaLinked)
            {
                _linked = false;
                break;
            }
        }

        if (_linked)
        {
            allLinked = isLocked = true;
            engine.allLinked = allLinked;
        }
        else
            isLocked = false;
    }

    void CreateBoard()
    {
        TextAsset _text = (TextAsset)Resources.Load("XML/00_Levels/lvl_" + levelName);
        XmlDocument _xdoc = new XmlDocument();
        _xdoc.LoadXml(_text.text);

        XmlNodeList _board = _xdoc.GetElementsByTagName("board");
        foreach (XmlNode _b in _board)
        {
            //Debug.Log("Board name=[" + _b.Attributes["name"].Value + "]");
			engine.levelName = _b.Attributes["name"].Value;
			//engine.nextLevel = _b.Attributes["next"].Value;

            XmlNodeList _hexas = _b.ChildNodes;
            foreach (XmlNode _h in _hexas)
            {
                Debug.Log("Hexa id=[" + _h.Attributes["id"].Value + "] parent=[" + _h.Attributes["parent"].Value + "]");
                GameObject _hexa = transform.FindChild(_h.Attributes["parent"].Value).FindChild("Hexa_" + _h.Attributes["id"].Value).gameObject;
                _hexa.GetComponent<HexaManager>().invokeWait = float.Parse(_h.Attributes["delay"].Value);
                _hexa.GetComponent<HexaManager>().status = 0;
                _hexa.SetActive(true);

                XmlNodeList _links = _h.ChildNodes;
                foreach (XmlNode _l in _links)
                {
                    //Debug.Log("Link id=[" + _l.Attributes["id"].Value + "] status=[" + _l.Attributes["status"].Value + "]");
                    _hexa.transform.FindChild("hexa_face").FindChild(_l.Attributes["id"].Value + "_side").GetComponent<HexaSideTrigger>().status = int.Parse(_l.Attributes["status"].Value);
                }
            }
        }
    }

    void SelectHexa()
    {
#if UNITY_WEBPLAYER || UNITY_EDITOR
                //Mouse control
                if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
                {
                    //Left Clicked
                    if (Input.GetMouseButtonDown(0))
                        ccw = false;

                    //Right Clicked
                    if (Input.GetMouseButtonDown(1))
                        ccw = true;

                    ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    if (Physics.Raycast(ray, out rayhit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Grid")))
                    {
                        rayhit.collider.gameObject.GetComponent<HexaManager>().RotateHexa(ccw);
                        moveCount++;
                    }
                }
#endif

#if UNITY_ANDROID || UNITY_IPHONE
        //Mobile touch control
        if (Input.touchCount > 0)
        {
            //****************************
            //*    Hexagon selection     *
            //****************************
            touch = Input.GetTouch(0);

            ray = Camera.main.ScreenPointToRay(touch.position);

            if (Physics.Raycast(ray, out rayhit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Grid")))
            {
                if (touch.phase == TouchPhase.Began)
                {
                    if (selectedHexa != null)
                    {
                        //Vector3 _tp = new Vector3(touch.position.x, touch.position.y, rayhit.collider.gameObject.transform.position.z);
						Vector3 _tp = Camera.main.ScreenToWorldPoint(touch.position);
						_tp.z = selectedHexa.transform.position.z;

                        beginDir = selectedHexa.transform.position - _tp;

						//debug_selectedHexa.text = string.Format("{0} pos={1}", selectedHexa.name, selectedHexa.transform.position);
                    }
                }

                if (touch.phase == TouchPhase.Stationary)
                {
                }

                if (touch.phase == TouchPhase.Moved)
                {
                    //Update the side upon rotation completion
                    UpdateSide();

                    if (selectedHexa != null && beginDir != Vector3.zero)
                    {
                        //Vector3 _tp = new Vector3(touch.position.x, touch.position.y, selectedHexa.transform.position.z);
						Vector3 _tp = Camera.main.ScreenToWorldPoint(touch.position);
						_tp.z = selectedHexa.transform.position.z;

						//debug_beginDir.text = string.Format("Touch={0}", touch.position);
						//debug_touchDir.text = string.Format("World={0}", _tp);

                        touchDir = selectedHexa.transform.position - _tp;
                        touchAngle = Vector3.Angle(touchDir, beginDir);

                        float _dx = touchDir.x - beginDir.x;
                        float _dy = touchDir.y - beginDir.y;

						//debug_touchSide.text = string.Format("Side={0}", touchSide);
						//debug_posDelta.text = string.Format("X=[{0}] Y=[{1}]", _dx, _dy);

                        if (touchAngle > thresholdAngle)
                        {
                            //upon rotating, update the beginDir as touchDir
                            beginDir = touchDir;

                            //rotates the hexa accordingly
                            if (touchSide)
                            {   //init touch is on the RIGHT side
                                if((_dx < 0 && _dy > 0) || ( _dx > 0 && _dy > 0))
                                    selectedHexa.GetComponent<HexaManager>().RotateHexa(false);
								else if((_dx < 0 && _dy < 0) || ( _dx > 0 && _dy < 0))
                                    selectedHexa.GetComponent<HexaManager>().RotateHexa(true);
                            }
                            else
                            {   //init touch is on the LEFT side
                                if ((_dx > 0 && _dy < 0) || (_dx < 0 && _dy < 0))
                                    selectedHexa.GetComponent<HexaManager>().RotateHexa(false);
								else if((_dx < 0 && _dy > 0) || (_dx > 0 && _dy > 0))
                                    selectedHexa.GetComponent<HexaManager>().RotateHexa(true);
                            }
                            hasMoved = true;
                        }
                    }
                }

                if (touch.phase == TouchPhase.Ended)
                {
                    if (!hasMoved)
                    {
                        if (rayhit.collider.gameObject.GetComponent<HexaManager>().status > -1)
                        {
                            if (selectedHexa != null)
                                selectedHexa.GetComponent<HexaManager>().status = 0;

                            selectedHexa = rayhit.collider.gameObject;
                            selectedHexa.GetComponent<HexaManager>().status = 1;
                            //touchLineRenderer.SetOriginObject(selectedHexa.transform);
                        }
                    }
                    //touchDir = Vector3.zero;
					beginDir = Vector3.zero;
                    hasMoved = false;
                }
            }
        }
#endif
    }

    private void UpdateSide()
    {
		Vector3 _pos = Camera.main.ScreenToWorldPoint (touch.position);

        if (_pos.x < selectedHexa.transform.position.x)
            touchSide = false;
        else
            touchSide = true;
    }

}
