﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TouchLineRenderer : MonoBehaviour {
    public Transform origin;
    //public Transform destination;

    public float lineDrawSpeed = 6f;

    public Text touchPosDisplay;

    private LineRenderer lineR;

    private float counter;
    private float dist;
    

	// Use this for initialization
	void Start () {
        lineR = GetComponent<LineRenderer>();
        lineR.SetWidth(1f, 1f);
        dist = 0f;

        if (origin != null)
            lineR.SetPosition(0, origin.position);
    }
	
	// Update is called once per frame
	void Update () {
        UpdateDestination();
	}

    void UpdateDestination()
    {
        if (Input.touchCount > 0 && origin!=null)
        {
            dist = Vector3.Distance(origin.position, Input.GetTouch(0).position);

            Touch _t = Input.GetTouch(0);
            Vector3 _v = new Vector3(_t.position.x, _t.position.y, origin.position.z);

            DrawTouchLine(_v);
        }
        else
        {
            dist = 0f;
            DrawTouchLine(Vector3.zero);
        }
    }

    void DrawTouchLine(Vector3 _touch_position)
    {
		_touch_position = Camera.main.ScreenToWorldPoint (_touch_position);

        counter += .1f / lineDrawSpeed;
        float _x = Mathf.Lerp(0, dist, counter);

        Vector3 _a;
        if (origin != null)
            _a = origin.position;
        else
            _a = Vector3.zero;

        //Get the unit vector in the desired direction, multiply by the desired length and add the starting point
        Vector3 _p = _x * Vector3.Normalize(_touch_position - _a) + _a;
        lineR.SetPosition(1, _p);
    }

    public void SetOriginObject(Transform _transform)
    {
        origin = _transform;

        if (_transform != null)
            lineR.SetPosition(0, origin.position);
        else
            lineR.SetPosition(0, Vector3.zero);
    }
}
