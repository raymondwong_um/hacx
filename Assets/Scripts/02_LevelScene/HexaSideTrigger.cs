﻿using UnityEngine;
using System.Collections;

public class HexaSideTrigger : MonoBehaviour
{

    public bool isDisabled = true;
    public bool isLinked = false;

    /* 0 = locked / unavailable
     * 1 = open
     * 2 = linked
     */
    public int status = 1;

	void Start()
	{
		transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y, 1f);
	}

    // Update is called once per frame
    void Update()
    {
        switch (status)
        {
            case 0:
                gameObject.SetActive(false);
                gameObject.tag = "HexaSideClose";
                isLinked = true;
                break;

            case 1:
                GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/00_Levels/Link_01_normal");
                gameObject.tag = "HexaSideOpen";
                isLinked = false;
                break;

            case 2:
                GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/00_Levels/Link_01_linked");
                isLinked = true;
                break;

        }
    }

    void OnCollisionEnter(Collision col)
    {
        Vector3 _contact = col.contacts[0].point;

        if (col.gameObject.tag.Equals("HexaSideOpen") && col.gameObject.GetComponent<HexaSideTrigger>().status > 0 && status > 0)
        {
            status = 2;

            if (!isDisabled)
            {
                GameObject _particle = CollideEffect();
                _particle.transform.position = _contact + _particle.transform.position;
            }
        }
    }

    void OnCollisionExit(Collision col)
    {
        status = 1;
    }

    private GameObject CollideEffect()
    {
        GameObject _particles = (GameObject)Instantiate(Resources.Load("VFX/CFXM_Hit_B Directional Blue") as GameObject);
        _particles.transform.position = new Vector3(0, _particles.transform.position.y, 0);
        _particles.SetActive(true);

        ParticleSystem _ps = _particles.GetComponent<ParticleSystem>();
        if (_ps != null && _ps.loop)
        {
            _ps.gameObject.AddComponent<CFX_AutoStopLoopedEffect>();
            _ps.gameObject.AddComponent<CFX_AutoDestructShuriken>();
        }

        return _particles;
    }

}
