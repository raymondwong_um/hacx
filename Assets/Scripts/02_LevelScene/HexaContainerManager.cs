﻿using UnityEngine;
using System.Collections;

public class HexaContainerManager : MonoBehaviour {
    public bool allHexaLinked = false;

    private HexaManager[] hexas;

	// Use this for initialization
	void Start () {
        hexas = GetComponentsInChildren<HexaManager>();
	}
	
	// Update is called once per frame
	void Update () {
        CheckHexa();
	}

    void CheckHexa()
    {
        bool _linked = true;
        foreach (HexaManager _hm in hexas)
        {
            if (!_hm.hexaLinked)
            {
                _linked = false;
                break;
            }
        }

        allHexaLinked = _linked;
    }
}
