﻿using UnityEngine;
using System.Collections;

public class EndPanelInvoker : MonoBehaviour {
	public GameObject endPanel;

	// Use this for initialization
	void Start () {
		float _delay = 0f;

		HexaEndEffectLine[] _lines = transform.GetComponentsInChildren<HexaEndEffectLine> ();
		_delay = (_lines.Length * 0.1f) + 1.5f;

		Invoke ("ActivateEndPanel", _delay);
	}
	
	private void ActivateEndPanel()
	{
		endPanel.SetActive (true);
	}
}
