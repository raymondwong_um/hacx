﻿using UnityEngine;
using System.Collections;

public class HexaEndEffect : MonoBehaviour {
    public Color beginColor;
    public Color endColor;

    private float duration = 0.5f;
    private float invokeWait;
    private float smoothness = 0.01f;

    //Sprite Colors
    private Color lerpedColor;

	// Use this for initialization
	void Start () {
        InitColor();

        invokeWait = transform.parent.GetComponent<HexaEndEffectLine>().invokeDelay;
        Invoke("StartEffect", invokeWait);
	}

    void Update()
    {
        UpdateEffect();
    }

    void InitColor()
    {
        beginColor = transform.FindChild("hexa_mask").gameObject.GetComponent<SpriteRenderer>().color;
        beginColor = Color.white;
        endColor = Color.black;
        endColor.a = 1f;
    }

    void StartEffect()
    {
        StartCoroutine(RunEffect());
    }

    void UpdateEffect()
    {
        transform.FindChild("hexa_mask").GetComponent<SpriteRenderer>().color = lerpedColor;
    }

    IEnumerator RunEffect()
    {
        float _progress = 0;
        float _inc = smoothness / duration;

        while (_progress < 1)
        {
            lerpedColor = Color.Lerp(beginColor, endColor, _progress);
            _progress += _inc;
            yield return new WaitForSeconds(smoothness);
        }
        lerpedColor = endColor;
        lerpedColor.a = 1f;
    }
}
