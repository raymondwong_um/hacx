﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelUIController : MonoBehaviour {
    public BoardManager bm;

    public void EndGame()
    {
        bm.isLocked = true;
        transform.FindChild("button_pause").GetComponent<Button>().interactable = false;
    }

    public void PauseGame()
    {
        bm.isLocked = true;
        transform.FindChild("PausePanel").gameObject.SetActive(true);
    }

}
