﻿using UnityEngine;
using System.Collections;

public class HexaEngine : MonoBehaviour
{
    public bool allLinked = false;

    public GameObject endEffect;
    public GameObject matrixBG;

    public LevelUIController levelUI;

    public int initData;
    public int remainData;
    public int moveCount = 0;

	public string levelName = "000";
    public string nextLevel = "001";

    private bool gameOver = false;

    private GameObject[] hexa;

    // Use this for initialization
    void Start()
    {
        remainData = initData;

    }

    // Update is called once per frame
    void Update()
    {
        remainData = initData - (moveCount * 5);
        if (remainData < 0)
            remainData = 0;

        if (allLinked && !gameOver)
            EndGame();
    }

    void EndGame()
    {
        gameOver = true;

        matrixBG.GetComponent<MatrixBGManager>().disabled = true;
        endEffect.SetActive(true);

        levelUI.EndGame();
    }

	public void NextLevel()
	{
		PlayerPrefs.SetString("LevelName", nextLevel);
		Application.LoadLevel("LevelScene_2D");
	}

	public void BackToMain()
	{
		Application.LoadLevel("LevelMap");
	}
}
