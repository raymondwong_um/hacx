﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MatrixBGManager : MonoBehaviour {
    public bool disabled = false;
    
    public GameObject matrixChild;

    private float screenH = 1136f;
    private float screenW = 640f;

    private int scale = 2;

	// Use this for initialization
	void Start () {
        StartCoroutine(GenMatrix());
    }
	
	// Update is called once per frame
	void Update () {
	}

    IEnumerator GenMatrix()
    {
        while(transform.childCount < 255 && !disabled){
            GameObject _matrixChild = Instantiate(matrixChild) as GameObject;
            _matrixChild.transform.SetParent(transform);
            _matrixChild.transform.localPosition = new Vector3(1, 1, 1);
            _matrixChild.transform.localRotation = new Quaternion(0, 0, 0, 0);

            _matrixChild.transform.localScale = new Vector3(1, 1, 1);
            _matrixChild.GetComponent<Text>().rectTransform.anchoredPosition = new Vector2(Random.Range(0, screenW), Random.Range(50, screenH));
            _matrixChild.GetComponent<MatrixTextManager>().scale = scale;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
