﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MatrixTextManager : MonoBehaviour
{
    public int scale = 1;
    
    private Color textColor;
    
    private int maxCharCount;
    
    private string matrixText = "";

	// Use this for initialization
	void Start () {
        maxCharCount = Random.Range(30, 80);

        textColor = new Color(158f / 255f, 212f / 255f, 253f / 255f, (float)Random.Range(25, 55) / 255f);
        this.GetComponent<Text>().fontSize = Random.Range(5 * scale, 11 * scale);
        StartCoroutine(GenText());
    }
	
	// Update is called once per frame
	void Update () {
        this.GetComponent<Text>().text = matrixText;
        this.GetComponent<Text>().color = textColor;
    }

    IEnumerator GenText()
    {
        while (matrixText.Length < maxCharCount)
        {
            string _st = "アエオウラレリロルワヲウ0123456789e";
            char _c = _st[Random.Range(0, _st.Length)];
            matrixText += _c;

            yield return new WaitForSeconds((float)(Random.Range(5, 20) * 0.01f));
        }
        StartCoroutine(FadeDestroy());
    }

    IEnumerator FadeDestroy()
    {
        float _a = this.GetComponent<Text>().color.a;
        while (this.GetComponent<Text>().color.a > 0)
        {
            _a -= 0.01f;
            textColor.a = _a;
            yield return new WaitForSeconds(0.1f);
        }
        Destroy(gameObject);
    }
}
